package test;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public final class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {

        String html = "<html><head><title>test</title></head>" +
                      "<body><p id=\"hello\">hello, world!</p></body></html>";
        Document document = Jsoup.parse(html);

        Element p = document.getElementById("hello");
        logger.info("{}", p.text());

        Element title = document.getElementsByTag("head").first();
        logger.info("{}", title.text());
        title.text("hello");
        logger.info("{}", title.text());

    }

}
